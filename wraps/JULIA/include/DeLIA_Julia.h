/**
 * @file DeLIA_Julia.h
 * @author Marcos Bandeira Irigoyen (mvbirigoyen@gmail.com)
 * @author Carla Santana (carla.ecomp@gmail.com)
 * @brief Main class of the DeLIA. Through it you can access the main features of the library in Julia
 * @version 0.1
 * @date 2023-10-18
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#include "jlcxx/jlcxx.hpp"
#include "../../../include/DeLIA.h"

void DeLIAJ_Init(int id, int comm_sz,
                std::string inputfile,
                std::string configfile);

bool DeLIAJ_SetGlobalData(int *global_data, int size);
bool DeLIAJ_SetGlobalData(double *global_data, int size);
bool DeLIAJ_SetGlobalData(char *global_data, int size);
bool DeLIAJ_SetGlobalData(float *global_data, int size);
bool DeLIAJ_SetGlobalData(bool *global_data, int size);
bool DeLIAJ_SetLocalData(int *local_data, int size);
bool DeLIAJ_SetLocalData(double *local_data, int size);
bool DeLIAJ_SetLocalData(char *local_data, int size);
bool DeLIAJ_SetLocalData(float *local_data, int size);
bool DeLIAJ_SetLocalData(bool *local_data, int size);
void DeLIAJ_SaveGlobalData();
void DeLIAJ_SaveLocalData();
bool DeLIAJ_ReadGlobalData();
bool DeLIAJ_ReadLocalData();
int  DeLIAJ_getCurrentGlobalIteration();
void DeLIAJ_HeartbeatMonitoring_Init();
void DeLIAJ_HeartbeatMonitoring_Finalize();
void DeLIAJ_Finalize();
bool DeLIAJ_CanWork();
bool DeLIAJ_CanRecoverGlobalCheckpointing();
bool DeLIAJ_TriggerWorking();
void DeLIAJ_SaveSettings();