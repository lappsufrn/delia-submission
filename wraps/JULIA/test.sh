SCRIPT="helloDelia.jl"
MODES="testReads testSignals testHM"
#if you have an alias of julia to call it from the shell, you'll need to change this variable
JULIAPATH="julia"

for MODE in $MODES
do
    $JULIAPATH $SCRIPT $MODE >/dev/null 2>&1
    if [ $(echo $?) = "0" ]
    then
        echo $MODE SUCCESS
    else
        echo $MODE FAIL
    fi
    rm data/*
done