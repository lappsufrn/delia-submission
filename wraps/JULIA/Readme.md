# DeLIA J
## Add CxxWrap to your Julia environment

Wheter you're using a global environment or a local one, you'll need to add it.  
Running `using Pkg; Pkg.add("CxxWrap")` should do the trick.

## Setting-up JLCxx

Put libcxxwrap-julia in the folder (wraps/JULIA) as in the following:

```sh
cd <PATH_TO_DELIA>/delia/wraps/JULIA
git clone https://github.com/JuliaInterop/libcxxwrap-julia.git
mkdir libcxxwrap-julia-build
cd libcxxwrap-julia-build
cmake -DJulia_PREFIX=<PATH_TO_JULIA> ../libcxxwrap-julia
cmake --build . --config Release
```

## Setting-up DeLIA_Julia

Now, make another folder inside wraps/JULIA for the build and build it:

```sh
cmake ../ -DCMAKE_INSTALL_PREFIX=<PATH_TO_DELIA>/delia/wraps/JULIA -DCMAKE_PREFIX_PATH=<PATH_TO_JULIA> -DJlCxx_DIR=<PATH_TO_DELIA>/delia/wraps/JULIA/JlCxx_lib
make -j
make install
```

p.s: Use `-DJlCxx_DIR=<PATH_TO_DELIA>/delia/wraps/JULIA/libcxxwrap-julia-build` if the first line isn't working.

## Testing the wrapper

The `helloDelia.jl` file is a script that, according to the parameter you give it, will test DeLIA's main functionalities. Before running it, make sure to create a `data` folder inside this folder. The exit code from the program will say if the test worked or not (use `echo $?` to print the last executed program's exit code). You can also run the shell script `test.sh` to test every functionality in sequence.

For running 'manually' the script, the command is `julia helloDelia.jl <TEST>` where TEST can be:
- 'testReads', for testing the save and read functions for local and global data;
- 'testSignals', for testing signal detection and handling;
- 'testHM', for testing the heartbeat monitoring through a forced fail;

## Reminders
- In ```helloDelia.jl```'s line 5 you'll need to change the path for yours build's.
- Don't forget to add CxxWrap's package to your Julia environment.
## References for Julia -> Cpp
https://github.com/JuliaInterop/CxxWrap.jl

https://github.com/JuliaInterop/libcxxwrap-julia