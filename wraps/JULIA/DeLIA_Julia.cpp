#include "include/DeLIA_Julia.h"

/**
 * @brief DEFINIR OS MODULOS AQUI ANTES
 * 
 * @param mod 
 * @return JLCXX_MODULE 
 */
JLCXX_MODULE define_julia_module(jlcxx::Module& mod) {
  mod.method("__cpp__DeLIAJ_Init", &DeLIAJ_Init);
  
  mod.method("DeLIAJ_SetGlobalData", (bool (*)(int*,int))&DeLIAJ_SetGlobalData);
  mod.method("DeLIAJ_SetGlobalData", (bool (*)(double*,int))&DeLIAJ_SetGlobalData);
  mod.method("DeLIAJ_SetGlobalData", (bool (*)(char*,int))&DeLIAJ_SetGlobalData);
  mod.method("DeLIAJ_SetGlobalData", (bool (*)(float*,int))&DeLIAJ_SetGlobalData);
  mod.method("DeLIAJ_SetGlobalData", (bool (*)(bool*,int))&DeLIAJ_SetGlobalData);

  mod.method("DeLIAJ_SetLocalData", (bool (*)(int*,int))&DeLIAJ_SetLocalData);
  mod.method("DeLIAJ_SetLocalData", (bool (*)(double*,int))&DeLIAJ_SetLocalData);
  mod.method("DeLIAJ_SetLocalData", (bool (*)(char*,int))&DeLIAJ_SetLocalData);
  mod.method("DeLIAJ_SetLocalData", (bool (*)(float*,int))&DeLIAJ_SetLocalData);
  mod.method("DeLIAJ_SetLocalData", (bool (*)(bool*,int))&DeLIAJ_SetLocalData);

  mod.method("DeLIAJ_SaveGlobalData", &DeLIAJ_SaveGlobalData);
  mod.method("DeLIAJ_SaveLocalData", &DeLIAJ_SaveLocalData);

  mod.method("DeLIAJ_ReadGlobalData", &DeLIAJ_ReadGlobalData);
  mod.method("DeLIAJ_ReadLocalData", &DeLIAJ_ReadLocalData);
  
  mod.method("DeLIAJ_HeartbeatMonitoring_Init", &DeLIAJ_HeartbeatMonitoring_Init);
  mod.method("DeLIAJ_HeartbeatMonitoring_Finalize", &DeLIAJ_HeartbeatMonitoring_Finalize);

  mod.method("DeLIAJ_getCurrentGlobalIteration", &DeLIAJ_getCurrentGlobalIteration);
  mod.method("DeLIAJ_Finalize", &DeLIAJ_Finalize);
  mod.method("DeLIAJ_CanWork", &DeLIAJ_CanWork);
  mod.method("DeLIAJ_CanRecoverGlobalCheckpointing", &DeLIAJ_CanRecoverGlobalCheckpointing);
  mod.method("DeLIAJ_TriggerWorking", &DeLIAJ_TriggerWorking);
  mod.method("DeLIAJ_SaveSettings", &DeLIAJ_SaveSettings);

}

void DeLIAJ_Init(int id, int comm_sz, std::string inputfile, std::string configfile){
    std::cout << "DeLIAJ_Init works " << std::endl;
    std::cout << "id " << id << std::endl;
    std::cout << "comm_sz " << comm_sz << std::endl;
    std::cout << "inputfile " << inputfile << std::endl;
    std::cout << "configfile " << configfile << std::endl;
    return DeLIA_Init(id, comm_sz, inputfile, configfile);
}

bool DeLIAJ_SetGlobalData(int *global_data, int size){
    return DeLIA_SetGlobalData(global_data, size, INT_CODE);
}
bool DeLIAJ_SetGlobalData(double *global_data, int size){
    return DeLIA_SetGlobalData(global_data, size, DOUBLE_CODE);
}
bool DeLIAJ_SetGlobalData(char *global_data, int size){
    return DeLIA_SetGlobalData(global_data, size, CHAR_CODE);
}
bool DeLIAJ_SetGlobalData(float *global_data, int size){
    return DeLIA_SetGlobalData(global_data, size, FLOAT_CODE);
}
bool DeLIAJ_SetGlobalData(bool *global_data, int size){
    return DeLIA_SetGlobalData(global_data, size, BOOL_CODE);
}

bool DeLIAJ_SetLocalData(int *local_data, int size){
    return DeLIA_SetLocalData(local_data, size, INT_CODE);
}
bool DeLIAJ_SetLocalData(double *local_data, int size){
    return DeLIA_SetLocalData(local_data, size, DOUBLE_CODE);
}
bool DeLIAJ_SetLocalData(char *local_data, int size){
    return DeLIA_SetLocalData(local_data, size, CHAR_CODE);
}
bool DeLIAJ_SetLocalData(float *local_data, int size){
    return DeLIA_SetLocalData(local_data, size, FLOAT_CODE);
}
bool DeLIAJ_SetLocalData(bool *local_data, int size){
    return DeLIA_SetLocalData(local_data, size, BOOL_CODE);
}

void DeLIAJ_HeartbeatMonitoring_Init(){
    return DeLIA_HeartbeatMonitoring_Init();
};
void DeLIAJ_HeartbeatMonitoring_Finalize(){
    return DeLIA_HeartbeatMonitoring_Finalize();
};

void DeLIAJ_SaveGlobalData() {
    return DeLIA_SaveGlobalData();
};
void DeLIAJ_SaveLocalData() {
    return DeLIA_SaveLocalData();
};
bool DeLIAJ_ReadGlobalData() {
    return DeLIA_ReadGlobalData();
};
bool DeLIAJ_ReadLocalData() {
    return DeLIA_ReadLocalData();
};
int DeLIAJ_getCurrentGlobalIteration() {
    return DeLIA_getCurrentGlobalIteration();
};
void DeLIAJ_Finalize() {
    return DeLIA_Finalize();
};
bool DeLIAJ_CanWork() {
    return DeLIA_CanWork();
};
bool DeLIAJ_CanRecoverGlobalCheckpointing() {
    return DeLIA_CanRecoverGlobalCheckpointing();
};
bool DeLIAJ_TriggerWorking() {
    return DeLIA_TriggerWorking();
};
void DeLIAJ_SaveSettings() {
    return DeLIA_SaveSettings();
};