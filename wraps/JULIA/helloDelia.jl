using Distributed
#remove it for use of the -p flag if you prefer 'julia -p Nworkers helloDelia.jl'
#or any different configuration (you'll need to add at least 2 processes)
Distributed.addprocs(2)

@everywhere module DeLIAJ
    using CxxWrap
    # ATUALIZE A PASTA
    @wrapmodule(joinpath([pwd(), "build","libDeLIAJ"]))

    function __init__()
        @initcxx
    end

    function DeLIAJ_Init(id::Integer, comm_sz::Integer, 
        inputfile::AbstractString, configfile::AbstractString)
        __cpp__DeLIAJ_Init(id, comm_sz, inputfile, configfile)
        atexit(()-> if DeLIAJ_TriggerWorking() DeLIAJ_SaveLocalData() end)
    end
end

@everywhere begin
    function testReads()
        res::Bool = true
        println("Changing every value stored in the global data pointers")
        gdpointers[1][] = 7
        gdpointers[2][] = 7.0
        gdpointers[3][] = 7.7
        #gdpointers[4][] = false
        #gdpointers[5][] = 'c'
        testVector[1] = 2.1
        testVector[2] = 3.2
        testVector[3] = 4.3
        
        for i in eachindex(gdpointers)
            println("Changed value in gdpointers[$(i)]:")
        end
        
        @show testVector

        DeLIAJ.DeLIAJ_ReadGlobalData()
        for i in eachindex(gdpointers)
            println("Value after ReadGlobal in gdpointers[$(i)]:")
            @show gdpointers[i][]
            if !(gdpointers[i][] == phgvalues[i]) res = false end
        end
        
        @show testVector
        if !(testVector == phvec) res = false end
        DeLIAJ.DeLIAJ_SaveLocalData()
        
        println("Changing every value stored in the local data pointers")
        ldpointers[1][] = 8
        ldpointers[2][] = 8.0
        ldpointers[3][] = 8.8
        #ldpointers[4][] = (mod(myid(),2)==0)
        #ldpointers[5][] = 'l'
        
        for i in eachindex(gdpointers)
            println("Changed value in ldpointers[$(i)]:")
            @show ldpointers[i][]
        end
        
        DeLIAJ.DeLIAJ_ReadLocalData()
        for i in eachindex(gdpointers)
            println("Value after ReadLocal in ldpointers[$(i)]:")
            @show ldpointers[i][]
            @show phlvalues[i]
            if !(ldpointers[i][] == phlvalues[i]) res = false end
        end
        return res
    end

    function testSignals()
        res::Bool = true
        println("Changing every value stored for trigger signal testing")
        ldpointers[1][] = 4
        ldpointers[2][] = 4.9
        ldpointers[3][] = 7.2
        for i in eachindex(ldpointers)
            println("Changed value in ldpointers[$(i)]:")
            @show ldpointers[i][]
            phlvalues[i] = ldpointers[i][]
        end

        #sending a SIGCONT to force TriggerSignal()'s SaveLocalData
        #SIGINT   2 — detects, saves, but julia doesn't seem to stop due to it
        #SIGCONT 18 — works perfectly
        #SIGSTOP 19 — Manages to detect, doesn't save
        #SIGTERM 15 — works perfectly, if you test it here notice that the
        # "od -t f <data>" should give 5.2, 7.2 and 7.2 respectively for each node
        #SIGTSTP 20 — works perfectly
        #if you happen to use MPI.jl for parallelization, a similar problem
        #   occurs with SIGTERM, but as MPI closes the other nodes it will send
        #   a SIGCONT to the other processes (?) and save every other node but
        #   the one that received it. See issue 19.
        
        Base.run(`kill -18 $(Base.Libc.getpid())`)

        println("Changing every value stored in the local data pointers again")
        ldpointers[1][] = 8
        ldpointers[2][] = 8.0
        ldpointers[3][] = 8.8

        for i in eachindex(ldpointers)
            println("Changed value in ldpointers[$(i)]:")
            @show ldpointers[i][]
        end

        DeLIAJ.DeLIAJ_ReadLocalData()
        for i in eachindex(gdpointers)
            println("Value after ReadLocal in ldpointers[$(i)]:")
            @show ldpointers[i][]
            if !(ldpointers[i][] == phlvalues[i]) res = false end
        end
        return res
    end

    function testHM()
        res::Bool = true
        println("Changing every value stored for HM testing")
        ldpointers[1][] = 99
        ldpointers[2][] = 21.3
        ldpointers[3][] = 50.2
        for i in eachindex(ldpointers)
            println("Changed value in ldpointers[$(i)]:")
            @show ldpointers[i][]
            phlvalues[i] = ldpointers[i][]
        end

        println("Not starting in worker 2 to force a hang from it")
        if myid()!=3
            DeLIAJ.DeLIAJ_HeartbeatMonitoring_Init()

            println("Sleeping for a heartbeat checkup")
            
            for i in range(0, stop=3)
                sleep(5)
            end

            DeLIAJ.DeLIAJ_HeartbeatMonitoring_Finalize()

            println("Changing every value stored in the local data pointers again")
            ldpointers[1][] = 8
            ldpointers[2][] = 8.0
            ldpointers[3][] = 8.8

            for i in eachindex(ldpointers)
                println("Changed value in ldpointers[$(i)]:")
                @show ldpointers[i][]
            end

            DeLIAJ.DeLIAJ_ReadLocalData()
            for i in eachindex(ldpointers)
                println("Value after ReadLocal in ldpointers[$(i)]:")
                @show ldpointers[i][]
                @show phlvalues[i]
                if !(ldpointers[i][] == phlvalues[i]) res = false end
            end
            DeLIAJ.DeLIAJ_HeartbeatMonitoring_Finalize()
        end
        println("final res: $res")
        return res
    end

    function Init()
        #julia starts process enumeration with 1
        DeLIAJ.DeLIAJ_Init(myid()-1, nprocs(), "params.json", "config.txt")

        #you could compare the hexdump (better, use od with the proper output flag) of node 0's local checkpoint with the global checkpoint for any difference
        #bool and char pointer are being considered UInt8 somehow
        global gdpointers = Ref{Int32}(1), Ref{Float32}(4.0), Ref{Float64}(5.2)#, Ref{UInt8}(true), Ref{UInt8}(91)
        global ldpointers = Ref{Int32}(myid()), Ref{Float32}( myid()+3.0) , Ref{Float64}(myid()+4.2)#, Ref{UInt8}(mod(myid(),2)==1), Ref{UInt8}(myid()+90)
        global testVector = Vector{Float64}([0.8, 83.2, 45.1])
        #placeholders for comparison
        #global phgvalues::Vector{Any}(undef, length(gdpointers))
        global phgvalues = [x[] for x in gdpointers]
        global phlvalues = [x[] for x in ldpointers]
        #global phlvalues::Vector{Any}(undef, length(ldpointers))
        global phvec = deepcopy(testVector)

        for i in eachindex(gdpointers)
            DeLIAJ.DeLIAJ_SetGlobalData(gdpointers[i],1)
            phgvalues[i] = gdpointers[i][]
            DeLIAJ.DeLIAJ_SetLocalData(ldpointers[i],1)
            phlvalues[i] = ldpointers[i][]
        end

        DeLIAJ.DeLIAJ_SetGlobalData(testVector, 3)
        phvec = deepcopy(testVector)

        if !DeLIAJ.DeLIAJ_CanWork()
            println("DeLIA can't work")
            exit(1)
        end
    end

end

@everywhere Init()
global Success = true

function executeTests()
    res = true
    MODE = ""
    try
        MODE = ARGS[1]
    catch e
        if (!isa(e, BoundsError))
            throw(e)
        end
        #this catch was only meant for invalid arguments
    end
    if MODE=="testReads"
        res = @distributed (&) for i in workers()
            testReads()
        end
        res = res & testReads()
    elseif MODE=="testSignals"
        res = @distributed (&) for i in workers()
            testSignals()
        end
        res = res & testSignals()
    elseif MODE=="testHM"
        @everywhere phres = testHM()
        res = @distributed (&) for i in workers()
            phres
        end
        res = res & phres
        sleep(5)
    else
        res = false
        if myid()==1 println("Invalid argument for arg") end
    end

    return res
end

if !DeLIAJ.DeLIAJ_CanRecoverGlobalCheckpointing()
    DeLIAJ.DeLIAJ_SaveSettings()
    @everywhere DeLIAJ.DeLIAJ_SaveGlobalData()
    Success = executeTests()
else
    println("DeLIA previous' data wasn't properly removed")
    Success = false
end

@everywhere DeLIAJ.DeLIAJ_Finalize()
#this usage of exit codes might impede direct adaptation for tests with MPI.jl
if (Success == true)
    exit(0)
else
    exit(1)
end