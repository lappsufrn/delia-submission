#!/bin/bash

# More details about the monitoring of the interruption signal in:
# https://aws.amazon.com/pt/blogs/compute/best-practices-for-handling-ec2-spot-instance-interruptions/

# Start application execution
sh execute.sh >outexecute.txt 2>errexecute.err &

TOKEN=`curl -s -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"`

while sleep 5; do

    HTTP_CODE=$(curl -H "X-aws-ec2-metadata-token: $TOKEN" -s -w %{http_code} -o /dev/null http://169.254.169.254/latest/meta-data/spot/instance-action)
    echo "$HTTP_CODE"
    if [ "$HTTP_CODE" -eq 401 ] ; then
        echo 'Refreshing Authentication Token'
        TOKEN=`curl -s -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 30"`
    elif [ "$HTTP_CODE" -eq 200 ] ; then
        # Insert Your Code to Handle Interruption Here
        echo "[MONITOR] Detected AWS interruption notice"
        # Send SIGTERM (15) to the application
        # DeLIA will detect and save the local data
        killall -s 15 AWSSPOT_EXAMPLE
        echo "[MONITOR] Sent SIGTERM to fwi at `date +"%H:%M:%S"`"
        # AWS Spot sends interruption signal 2min before
        sleep 120
    else
        echo 'Not Interrupted'
    fi
done