#!/bin/bash

# =============================================
# The path to the folder where DeLIA will save
# the data should persistent, in this example
# the folder is insite the project
# =============================================

DELIA_PATH="<path_to_delia>" #example /data/workspace/delia

cd $DELIA_PATH
wait;
if [ -e $DELIA_PATH/build ]; then
    rm -rv ./build/*; wait  
    rm -r build
    echo "DELETING BUILD"
fi

if [ -e $DELIA_PATH/DeLIA_lib ]; then
    rm -rv ./DeLIA_lib/*; wait  
    rm -r DeLIA_lib
    echo "DELETING DeLIA_lib"
fi

mkdir build 
cd build
make clear
cmake ../ -DCMAKE_INSTALL_PREFIX=$DELIA_PATH -DVERBOSE=ON -DTEST=ON -DDEBUG=OFF;
make -j; 
make install;

AWS_TEST_PATH="<path_to_project_example>" #example /data/workspace/delia/examples/AWS_Spot/project

cd $AWS_TEST_PATH
if [ -e $AWS_TEST_PATH/build ]; then
    rm -rv ./build/*; wait    
    rm -r build
    echo "DELETING BUILD"
fi

mkdir DeLIA_lib
cp -r $DELIA_PATH/DeLIA_lib/ $AWS_TEST_PATH/

mkdir build
cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=$AWS_TEST_PATH -DDELIA=ON -DDEBUG=OFF;
make -j; 
make install;