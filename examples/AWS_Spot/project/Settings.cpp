#include "Settings.h"

Settings::Settings(std::string config_file){
    Json::Value root;
    try
    {
        std::ifstream file("config_file.json");
        file >> root;
        if (!root["SIZE"].isNull()) {
            n = root["SIZE"].asInt();
        } else {
            n = 10;
        }

        if (!root["LOCAL_ITERATIONS"].isNull()) {
            iterations = root["LOCAL_ITERATIONS"].asInt();
        } else {
            iterations = 10;
        }
        std::cout << "Config file valid" << std::endl;

    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        std::cerr << "Config file invalid" << std::endl;
    }
}