#ifndef SETTINGS_H
#define SETTINGS_H

#include <fstream>
#include <iostream>
#include <string>
#include "../../../libs/jsoncpp/json.h"
#include "../../../libs/jsoncpp/json-forwards.h"

class Settings {
    private:
        int n, iterations;
    public:
        Settings(std::string config_file);
        int getN(){return n;}
        int getIterations(){return iterations;}
        ~Settings(){};
};
#endif