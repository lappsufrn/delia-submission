/**
 * @file Main.cpp
 * @author Carla Santana (carla.ecomp@gmail.com)
 * @brief Simple example to test DeLIA features in 
 * AWS Spot. In this example we have an infinit loop 
 * that will save the data untill interrupted
 * @version 0.1
 * @date 2023-11-29
 * 
 * @copyright Copyright (c) 2023 
 */
#include <iostream>
#include <string>
#include <mpi.h>
#include <unistd.h>
#include "DeLIA.h"
#include "Settings.h"

int main(int argc, char **argv) {

   int n, iterations, ind=0;
   int rank, comm_sz;
   double *aux_data, *local_data, *global_data;
   Settings *st;

   MPI_Init(&argc, &argv);
   MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   
   st = new Settings("config_file.json");
   n = st->getN();
   iterations = st->getIterations();

   aux_data    = new double[n];
   local_data  = new double[n];
   global_data = new double[n];

   if (local_data == NULL || global_data == NULL) {
      fprintf(stderr, "Can't allocate vectors\n");
      exit(-1);
   }

   DeLIA_Init(rank, comm_sz, "delia_param.json", "config_file.json");
   if (!DeLIA_CanWork()) {
      std::cerr << "DeLIA can not work" << std::endl;
      exit;
   };
   DeLIA_SetGlobalData(global_data, n, DOUBLE_CODE);
   DeLIA_SetLocalData(local_data, n, DOUBLE_CODE);

   for (int i = 0; i < n; i++) {
      local_data[i] = 0;
   }

   // Check if there are data to be read with the current settings
   bool canRecover = DeLIA_CanRecoverGlobalCheckpointing();
   MPI_Barrier(MPI_COMM_WORLD);
   if (canRecover)
      DeLIA_ReadGlobalData();
   else
      DeLIA_SaveSettings();
   ind = DeLIA_getCurrentGlobalIteration();
   
   while (1 == 1) {
      
      for (int j = 0; j < iterations; j++) {
         for (int i = 0; i < n; i++) {
            aux_data[i] = rank*i*ind;
            sleep(1);
         }

         for (int i = 0; i < n; i++) {
            local_data[i] += aux_data[i];
         }
      }
      MPI_Allreduce(local_data, global_data, n, MPI_DOUBLE, 
                     MPI_SUM, MPI_COMM_WORLD);
      if (rank ==0 ) 
         printf("Iteration %d ends\n", ind);
     DeLIA_SaveGlobalData();
     ind++;
   }

   DeLIA_Finalize();
   MPI_Finalize();
   delete (local_data);
   delete (global_data);
}