#!/bin/bash

DELIA_PATH=$(cd ../..  && pwd)
# DELIA_PATH="<path_to_delia>" #example /data/workspace/delia

QUICK_START_PATH=$DELIA_PATH/examples/QuickStart/project
# QUICK_START_PATH="<path_to_project_example>" #example /data/workspace/delia/examples/QuickStart/project
if ! [ -e $QUICK_START_PATH ]
then
    echo "Invalid path, execute the script from it's folder"
    exit 1
fi

cd $DELIA_PATH
wait;
if [ -e $DELIA_PATH/build ]; then
    rm -rv ./build/*; wait  
    rm -r build
    echo "DELETING BUILD"
fi

if [ -e $DELIA_PATH/DeLIA_lib ]; then
    rm -rv ./DeLIA_lib/*; wait  
    rm -r DeLIA_lib
    echo "DELETING DeLIA_lib"
fi

mkdir build 
cd build
make clear
cmake ../ -DCMAKE_INSTALL_PREFIX=$DELIA_PATH -DVERBOSE=ON -DTEST=ON -DDEBUG=OFF;
make -j; 
make install;

cd $QUICK_START_PATH
if [ -e $QUICK_START_PATH/build ]; then
    rm -rv ./build/*; wait    
    rm -r build
    echo "DELETING BUILD"
fi

mkdir DeLIA_lib
cp -r $DELIA_PATH/DeLIA_lib/ $QUICK_START_PATH/

mkdir build
cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=$QUICK_START_PATH -DDELIA=ON -DDEBUG=OFF;
make -j; 
make install;