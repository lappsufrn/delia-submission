/**
 * @file Main.cpp
 * @author Carla Santana (carla.ecomp@gmail.com)
 * @brief Simple example to understand how DeLIA works
 * @version 0.1
 * @date 2023-11-29
 * 
 * @copyright Copyright (c) 2023 
 */
#include <iostream>
#include <string>
#include <mpi.h>
#include <unistd.h>
#include "DeLIA.h"
#include "Settings.h"

int main(int argc, char **argv) {

   int data_size, iterations, ind=0;
   int rank, comm_sz;
   double *local_data, *global_data;
   Settings *st;

   MPI_Init(&argc, &argv);
   MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   
   // Class which represents the application configuration
   st = new Settings("config_file.json");
   // Size of the data
   data_size = st->getDataSize();
   // Iterations number
   iterations = st->getIterations();

   // Data that will be calculated in each process
   local_data  = new double[data_size];
   // Data that will be result of the reduction of the local data
   global_data = new double[data_size];

   if (local_data == NULL || global_data == NULL) {
      fprintf(stderr, "Can't allocate vectors\n");
      exit(-1);
   }

   // Initialization passing the rank of the process, the number of processes
   // the configuration file from DeLIA
   // the configuration file from Application
   DeLIA_Init(rank, comm_sz, "delia_param.json", "config_file.json");
   if (!DeLIA_CanWork()) {
      std::cerr << "DeLIA can not work" << std::endl;
      exit;
   };

   // Passing the global and local data to DeLIA
   DeLIA_SetGlobalData(global_data, data_size, DOUBLE_CODE);
   DeLIA_SetLocalData(local_data, data_size, DOUBLE_CODE);
   
   // Check if there are data to be read with the current settings
   bool canRecover = DeLIA_CanRecoverGlobalCheckpointing();
   
   // Barrier necessary because if there aren't global data
   // to be recover all process should check in the same moment 
   MPI_Barrier(MPI_COMM_WORLD);
   if (canRecover)
      DeLIA_ReadGlobalData();
   else
      DeLIA_SaveSettings();
   ind = DeLIA_getCurrentGlobalIteration();

   // Initialization of the Hearbeat monitoring
   DeLIA_HeartbeatMonitoring_Init();
   
   for (ind; ind < iterations; ind++) {

      for (int i = 0; i < data_size; i++) {
         // Calculation of local data
         local_data[i] = rank*i*ind;
      }
      // Reduction of the data
      MPI_Allreduce(local_data, global_data, data_size, MPI_DOUBLE, 
                     MPI_SUM, MPI_COMM_WORLD);
      if (rank ==0 ) 
         printf("Iteration %d ends\n", ind);

      // DeLIA will save the global data according the frequency already 
      // informed in the delia_params.json
      DeLIA_SaveGlobalData();
   }

   DeLIA_HeartbeatMonitoring_Finalize();
   DeLIA_Finalize();
   MPI_Finalize();
   delete (local_data);
   delete (global_data);
}