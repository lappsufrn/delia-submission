#ifndef SETTINGS_H
#define SETTINGS_H

#include <fstream>
#include <iostream>
#include <string>
#include "../../../libs/jsoncpp/json.h"
#include "../../../libs/jsoncpp/json-forwards.h"

class Settings {
    private:
        int data_size, iterations;
    public:
        Settings(std::string config_file);
        int getDataSize(){return data_size;}
        int getIterations(){return iterations;}
        ~Settings(){};
};
#endif