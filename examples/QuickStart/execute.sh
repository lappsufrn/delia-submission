#!/bin/bash

TEST_PATH=$(pwd)/project
# TEST_PATH="<path_to_project_example>"

if [ -e $TEST_PATH ]
then
    cd $TEST_PATH
    if ! [ -d ./data ]
    then
        mkdir data
    fi
    # IN THIS SCRIPT WE JUST EXECUTE THE EXAMPLE WITH 2 PROCESSES
    time mpirun -n 2 $TEST_PATH/bin/QUICKSTART_EXAMPLE
else
echo "Invalid path, execute the script from it's folder"
fi